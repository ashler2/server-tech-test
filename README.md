# Korelogic Backend Test

A REST API to provide the user with information regarding restaurants created with TDD.

## Authors

**Ashley Readman-Thorley**

## Built with

- Node

- Express

## Getting Started

To get a copy of the project up and running on your local machine for development and testing purposes, please follow the below instructions

### Prerequisites

Node
Git

### Installing

Once the prerequisites have been met, the following instructions will help you install the repository.

In the command line -

git clone https://ashler2@bitbucket.org/ashler2/server-tech-test.git

To install the dependencies and start the server

```
npm install
```

then

```
npm run start
```

## Usage

Once the server has started on localhost:9090, the following endpoints will be avaliable to the user:

### Get /api/restaurants - Provides all restaurants

Optional Queries - GET /api/restaurants?vegan-options=true&dog-friendly=false

- vegan-options - true/false
- dog-friendly - true/false
- cuisines - cuisine type i.e /api/restaurants?cuisines=british,cafe

### Get /api/restaurants/1 - Provides the user with a restaurant by id

## Running the Tests

This project uses Mocha, Chai and supertest to test the code
Once the server is running, to run the tests use the following command

```
npm test
```
