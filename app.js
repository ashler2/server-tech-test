const express = require("express");
const app = express();
const { apiRouter } = require("./routers/apiRouters");
const { send404 } = require("./error/error");

app.use(express.json());

app.use("/api", apiRouter);

app.use(send404);
app.all(send404);

module.exports = { app };
