const {
  fetchRestaurants,
  fetchRestaurantById
} = require("../models/restaurantModel");

exports.getRestaurants = (req, res, next) => {
  try {
    const params = req.query;
    return res.status(200).send(fetchRestaurants(params));
  } catch (error) {
    next(error);
  }
};

exports.getRestaurantById = (req, res, next) => {
  try {
    const { id } = req.params;

    return res.status(200).send(fetchRestaurantById(id));
  } catch (err) {
    next(err);
  }
};
