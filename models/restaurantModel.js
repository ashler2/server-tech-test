const restaurantData = require("../restaurants.json");
const { dogCheck, veganCheck, cuisineCheck } = require("../utils/utils");
exports.fetchRestaurants = params => {
  if (Object.keys(params).length === 0) {
    return restaurantData;
  } else {
    let restaurantDataFiltered = restaurantData;
    const dog = params["dog-friendly"];
    const vegan = params["vegan-options"];
    const cuisines = params.cuisines;
    if (dog !== undefined) {
      restaurantDataFiltered = dogCheck(dog, restaurantDataFiltered);
    }
    if (vegan !== undefined) {
      restaurantDataFiltered = veganCheck(vegan, restaurantDataFiltered);
    }
    if (cuisines !== undefined) {
      restaurantDataFiltered = cuisineCheck(cuisines, restaurantDataFiltered);
    }

    return restaurantDataFiltered;
  }
};
exports.fetchRestaurantById = id => {
  const restaurant = restaurantData.filter(restaurant => {
    if (restaurant.id === Number(id)) {
      return restaurant;
    }
  });
  if (restaurant.length !== 0) {
    return restaurant[0];
  } else {
    throw { status: 404, msg: "Error 404 - ID not found" };
  }
};
