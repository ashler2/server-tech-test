const express = require("express");
const apiRouter = express.Router();
const { restaurantRouter } = require("../routers/restaurantRouter");

apiRouter.use("/restaurants", restaurantRouter);

// apiRouter.use("/*", send404);

module.exports = { apiRouter };
