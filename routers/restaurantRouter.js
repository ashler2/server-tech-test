const express = require("express");
const restaurantRouter = express.Router();
const {
  getRestaurants,
  getRestaurantById
} = require("../controllers/restaurantController");

restaurantRouter.get("/", getRestaurants);

restaurantRouter.get("/:id", getRestaurantById);

module.exports = { restaurantRouter };
