const chai = require("chai");
const { app } = require("../app");
const { expect } = chai;
const request = require("supertest")(app);

describe("Tests for models", () => {
  describe("/restaurants", () => {
    it("returns an array of restaurants unfiltered when no query is provided", () => {
      return request
        .get("/api/restaurants")
        .expect(200)
        .then(({ body }) => {
          expect(body.length).to.equal(6);
          expect(body[0]).to.have.keys(
            "id",
            "name",
            "address",
            "cuisine",
            "rating",
            "dog-friendly",
            "vegan-options"
          );
          expect(body).to.be.an("array");
        });
    });
    it("returns an array of restaurants filtered when dog query is true", () => {
      return request
        .get("/api/restaurants?dog-friendly=true")
        .expect(200)
        .then(({ body }) => {
          expect(body[0]["dog-friendly"]).to.equal(true);
          expect(body).to.be.an("array");
        });
    });
    it("returns an array of restaurants filtered when dog query is false", () => {
      return request
        .get("/api/restaurants?dog-friendly=false")
        .expect(200)
        .then(({ body }) => {
          expect(body[0]["dog-friendly"]).to.equal(false);
          expect(body).to.be.an("array");
        });
    });
    it("returns an array of restaurants filtered when vegan query is true", () => {
      return request
        .get("/api/restaurants?vegan-options=true")
        .expect(200)
        .then(({ body }) => {
          expect(body[0]["vegan-options"]).to.equal(true);
          expect(body).to.be.an("array");
        });
    });
    it("returns an array of restaurants filtered when vegan query is false", () => {
      return request
        .get("/api/restaurants?vegan-options=false")
        .expect(200)
        .then(({ body }) => {
          expect(body[0]["vegan-options"]).to.equal(false);
          expect(body).to.be.an("array");
        });
    });
    it("returns an array of restaurants filtered when vegan and dog query is true", () => {
      return request
        .get("/api/restaurants?vegan-options=true&dog-friendly=true")
        .expect(200)
        .then(({ body }) => {
          expect(body[0]["vegan-options"]).to.equal(true);
          expect(body[0]["dog-friendly"]).to.equal(true);

          expect(body).to.be.an("array");
        });
    });
    it("returns an array of restaurants filtered cuisine query=british,cafe", () => {
      return request
        .get("/api/restaurants?cuisines=british,cafe")
        .expect(200)
        .then(({ body }) => {
          expect(body).to.be.an("array");
          expect(body.length).to.eql(2);
          expect(body[0].cuisine).to.includes("British", "Cafe");
        });
    });
  });
  describe("/restaurants/:id", () => {
    it("returns an restaurant by id", () => {
      return request
        .get("/api/restaurants/2")
        .expect(200)
        .then(({ body }) => {
          expect(body).to.have.keys(
            "id",
            "name",
            "address",
            "cuisine",
            "rating",
            "dog-friendly",
            "vegan-options"
          );
          expect(body).to.be.an("object");
        });
    });

    it("returns an error 404 for restaurant by id when id doesn't exist", () => {
      return request
        .get("/api/restaurants/2000")
        .expect(404)
        .then(({ body }) => {
          expect(body).to.have.keys("status", "msg");
          expect(body.msg).to.eql("Error 404 - ID not found");
        });
    });
  });
});
