const dogCheck = (dog, array) => {
  if (dog.toLowerCase() === "true") {
    return array.filter(item => {
      if (item["dog-friendly"] === true) {
        return item;
      }
    });
  }
  if (dog.toLowerCase() === "false") {
    return array.filter(item => {
      if (item["dog-friendly"] === false) {
        return item;
      }
    });
  }
};

const veganCheck = (vegan, array) => {
  if (vegan.toLowerCase() === "true") {
    return array.filter(item => {
      if (item["vegan-options"] === true) {
        return item;
      }
    });
  }
  if (vegan.toLowerCase() === "false") {
    return array.filter(item => {
      if (item["vegan-options"] === false) {
        return item;
      }
    });
  }
};

const checker = (arr, test) => test.every(type => arr.includes(type));
const cuisineCheck = (cuisines, array) => {
  const cuisineArray = cuisines.split(",").map(item => {
    return item.toLowerCase();
  });

  return array.filter(restaurant => {
    const cuisine = restaurant.cuisine.map(item => {
      return item.toLowerCase();
    });

    if (checker(cuisine, cuisineArray)) {
      return restaurant;
    }
    if (cuisineArray.length === 1) {
      if (cuisine.includes(cuisineArray[0])) {
        return restaurant;
      }
    }
  });
};
module.exports = { dogCheck, veganCheck, cuisineCheck };
